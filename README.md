## Intallation

Go to root path of project and then run command 
`# npm install`
`# npm install -g webpack webpack-dev-server cross-env`

* You can lunch that on development mode `npm run dev`
* You can lunch that on production mode `npm run build`

## Define Max filesize

maxSize	number	-  (number) Maximum file size
minSize	number	-  (number)	Minimum file size

.... 
Other useful props are available on https://react-dropzone.js.org/

## Call Endpoint API after Upload File

You can define custom AJAX code at 80 lines of Component/StatusBar.js

