import React, { Component } from 'react';

const uuidv1 = require('uuid/v1');

class Thumbnail extends Component {
  constructor() {
    super();

    this.state = {
      uuid: uuidv1(),
    };
  }
  componentDidMount() {
    this.drawCanvas(this.props);
    // this.convertCanvasToFormData();
  }
  componentWillReceiveProps(newProps) {
    if (this.props.src != newProps.src) {
      this.drawCanvas(newProps);
    }
  }
  drawCanvas(props) {
    const self = this;
    const canvas = document.getElementById(this.state.uuid);
    const ctx = canvas.getContext('2d');
    let img = null;
    img = new Image();
    img.onload = function () {
      canvas.height = canvas.width * (img.height / img.width);
      let oc = document.createElement('canvas'),
        octx = oc.getContext('2d');

      oc.width = img.width * 0.5;
      oc.height = img.height * 0.5;
      octx.drawImage(img, 0, 0, oc.width, oc.height);

      ctx.drawImage(
        oc, 0, 0, oc.width, oc.height,
        0, 0, canvas.width, canvas.height,
      );
      self.convertCanvasToFormData();
    };
    img.src = props.src;
  }
  dataURItoBlob(dataURI) {
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }


    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
  }

  convertCanvasToFormData() {
    const dataURL = document.getElementById(this.state.uuid).toDataURL();
    const blob = this.dataURItoBlob(dataURL);
    this.props.makeThumbnail(blob);
  }
  render() {
    const { width, src } = this.props;
    return (
      <canvas id={this.state.uuid} width={width} style={{ display: 'none' }} />
    );
  }
}

export default Thumbnail;
