
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from './Header';
import StatusBar from './StatusBar';

class Container extends Component {
  render() {
    const { files } = this.props.data;

    return (
      <div className="main-container">
        <Header />
        {
            files.map((file, i) => <StatusBar key={i} index={i} file={file} />)
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.common,
});

Container.propTypes = {
  data: PropTypes.object,
};
export default connect(mapStateToProps)(Container);
