import React, { Component } from 'react';
import { ProgressBar, Col, Row, Image, ButtonToolbar, Button } from 'react-bootstrap';
import axios from 'axios';
import { connect } from 'react-redux';
import Thumbnail from './Thumbnail';
import attachmentIcon from '../assets/imgs/attach_icon.png';
import Overlay from './Overlay';

class StatusBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      percentCompleted: 0,
      source: axios.CancelToken.source(),
      thumbnail: null,
      thumbPercentCompleted: 0,
      isCompleted: false,
      isStart: false,
      isCancel: false,
      isRemove: false,
    };
    this.onUploadHandler = this.onUploadHandler.bind(this);
    this.onCancelUploadHandler = this.onCancelUploadHandler.bind(this);
    this.makeThumbnail = this.makeThumbnail.bind(this);
    this.onRemoveHandler = this.onRemoveHandler.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.data.start_upload && !this.state.isStart) {
      this.onUploadHandler();
      this.setState({ isStart: true });
    }
    if (newProps.data.cancel_upload && !this.state.isCancel) {
      // newProps.CancelFileUpload();
      this.onCancelUploadHandler();
    }
  }

  onCancelUploadHandler() {
    const source = this.state.source;

    if (this.state.isCompleted || !this.state.isStart) return;
    source.cancel('request is cancelled');
    this.setState({
      percentCompleted: 0,
      thumbPercentCompleted: 0,
      source: axios.CancelToken.source(),
      isCancel: true,
      isStart: false,
    });
    // set progress to 0 when click local cancel button
    this.props.ResetTotalProgress();
  }

  onUploadHandler() {
    const { isCompleted, isStart } = this.state;
    const { type } = this.props.file;
    const uniqueFilename = `${Date.now()}.${this.props.file.name.split('.').pop()}`;


    // determine wheter uploading is already started or completed
    if (isCompleted || isStart) return;

    this.setState({ isStart: true, isCancel: false });
    this.props.Removable(false); // check wheter all items are removale
    this.uploadFile(uniqueFilename);
    if (type == 'image/png' || type == 'image/jpeg') { this.upLoadThumbnail(uniqueFilename); }
  }

  onRemoveHandler() {
    this.onCancelUploadHandler();
    this.props.deleteCurrentFile(this.props.index);
  }

  uploadFile(uniqueFilename) {
    const self = this;
    const data = this.makeFormData(null, uniqueFilename);
    axios.post('http://s3.amazonaws.com/alexandr-dev-bucket', data.formData, data.config).then((res) => {
      self.setState({ isCompleted: true, isStart: false, isRemove: true });
      this.props.ResetTotalProgress();
      self.props.StartFileUpload(false);
    }).catch((err) => {
      if (axios.isCancel(err)) {
        console.log('Request canceled', err.message);
      } else {}
    });
  }

  makeThumbnail(thumbnail) {
    this.setState({ thumbnail });
  }

  upLoadThumbnail(uniqueFilename) {
    const data = this.makeFormData(this.state.thumbnail, uniqueFilename);
    axios.post('http://s3.amazonaws.com/alexandr-dev-bucket', data.formData, data.config).then((res) => {
      // console.log(res);
    }).catch((err) => {
      if (axios.isCancel(err)) {
        console.log('Request canceled', err.message);
      } else {}
    });
  }

  makeFormData(thumbnail = null, uniqueFilename) {
    const { name, size, preview } = this.props.file;
    const formData = new FormData();
    const self = this;

    formData.append('acl', 'public-read');
    formData.append('AWSAccessKeyId', 'AKIAIOHDMS73V2QDWQYA');
    formData.append('policy', 'ewogICJleHBpcmF0aW9uIjogIjIwMTgtMDItMDFUMDA6MDA6MDBaIiwKICAiY29uZGl0aW9ucyI6IFsKICAgIHsiYnVja2V0IjogImFsZXhhbmRyLWRldi1idWNrZXQiIH0sCiAgICB7ImFjbCI6ICJwdWJsaWMtcmVhZCIgfSwKICAgIFsic3RhcnRzLXdpdGgiLCAiJGtleSIsIiJdLAogIF0KfQo=');
    formData.append('signature', 'FHMeuNM/Ck6U2fJPhgKruWZL9xA=');

    if (thumbnail != null) {
      formData.append('key', `thumbnail/thumb-${uniqueFilename}`);
      formData.append('file', thumbnail);
    } else {
      formData.append('key', `data/${uniqueFilename}`);
      formData.append('file', this.props.file);
    }

    const config = {
      onUploadProgress(progressEvent) {
        // return none if stop upload
        if (self.state.isCancel || self.state.isStart == false) {
          return;
        }
        const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
        if (thumbnail != null) {
          self.setState({ thumbPercentCompleted: percentCompleted });
        } else {
          self.setState({ percentCompleted });
        }
        const index = self.props.index;
        self.props.SetTotalProgress({ index, percentCompleted });
      },
      cancelToken: this.state.source.token,
    };
    return { formData, config };
  }

  render() {
    const { name, size, preview } = this.props.file;
    const total_size = Math.round(size / 1024);
    const currentSpeed = Math.round(total_size / 100 * this.state.percentCompleted);
    return (
      <Row className="show-grid" style={{ display: this.state.isRemove ? 'none' : 'block' }}>
        <Overlay isShow={!!this.state.isStart} isCompleted={this.state.isCompleted} />
        <Col md={2} style={{ padding: 20 }}>
          {
            (this.props.file.type == 'image/png' || this.props.file.type == 'image/jpeg') ?
              <Image src={preview} height={100} />
          : <Image src={attachmentIcon} height={50} />
          }
        </Col>
        <Col md={4}>
          <div style={{ textAlign: 'center', lineHeight: '70px' }}><b>{name}</b></div>
        </Col>
        <Col md={3} style={{ lineHeight: '70px', textAlign: 'center' }}>
          <div>
            {!this.state.isCompleted ?
            `${currentSpeed} / ${total_size} Kb ` + ` - ${`${this.state.percentCompleted}%`}`
             : <div style={{ color: 'red' }}>Completed !</div>
            }
          </div>
          <div><ProgressBar now={this.state.percentCompleted} /></div>

          {
            (this.props.file.type === 'image/png' || this.props.file.type === 'image/jpeg') ?
              <Thumbnail src={preview} width={200} makeThumbnail={this.makeThumbnail} />
          : null
          }
        </Col>
        <Col md={3} style={{ paddingTop: '20px' }}>
          <ButtonToolbar>
            {!this.state.isCompleted ?
              <Button
                type="submit"
                bsStyle="primary"
                onClick={this.onUploadHandler}
                disabled={!!this.state.isStart}
              >
            Start
              </Button>
            : null
            }
            {this.state.isStart && !this.props.data.start_upload ?
              <Button
                bsStyle="success"
                onClick={this.onCancelUploadHandler}
                disabled={!this.state.isStart}
              >Cancel
              </Button>
            : null
            }
            {!this.state.isStart ?
              <Button
                bsStyle="danger"
                onClick={this.onRemoveHandler}
                disabled={!!this.props.data.start_upload}
              >Remove
              </Button>
            : null
            }
          </ButtonToolbar>
        </Col>


      </Row>
    );
  }
}

const mapStateToProps = state => ({
  data: state.common,
});
const mapDispatchToProps = dispatch => ({
  deleteCurrentFile: (index) => {
    dispatch({
      type: 'DELETE_CURRENT_FILE',
      value: index,
    });
  },
  SetTotalProgress: (value) => {
    dispatch({
      type: 'SET_TOTAL_PROGRESS',
      value,
    });
  },
  StartFileUpload: value => dispatch({
    type: 'START_FILE_UPLOAD',
    value,
  }),
  CancelFileUpload: value => dispatch({
    type: 'CANCEL_FILE_UPLOAD',
    value,
  }),
  ResetTotalProgress: () => dispatch({
    type: 'RESET_TOTAL_PROGRESS',
  }),
  Removable: value => dispatch({
    type: 'IS_REMOVABLE',
    value,
  }),
});
export default connect(mapStateToProps, mapDispatchToProps)(StatusBar);
