import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';
import { Row, Col, ButtonToolbar, Button, ProgressBar } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { setFiles } from '../actions';

class Header extends Component {
  constructor() {
    super();

    this.state = {
      total_progress: 0,
    };
    this.onDrop = this.onDrop.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    let sum = 0;
    console.log('next Props:', nextProps.data.total_progress);
    // if (!nextProps.data.start_upload) {
    //   this.setState({ total_progress: 0 });
    // }
    nextProps.data.total_progress.forEach((data) => {
      sum += data.percentCompleted;
    });
    let total_progress = Math.round(sum / nextProps.data.total_progress.length);
    total_progress = nextProps.data.total_progress.length === 0 ? 0 : total_progress;
    this.setState({ total_progress });
  }
  onDrop(files) {
    const { SetFileToStore } = this.props;
    SetFileToStore(files);
  }

  render() {
    return (
      <div>
        <Row>
          <Col md={6} xs={12}>
            <ButtonToolbar>
              <Button
                bsStyle="primary"
                onClick={() => { this.dropzoneRef.open(); }}
              >
              Add Files
              </Button>
              <Button
                bsStyle="success"
                onClick={() => this.props.StartFileUpload(true)}
                // disabled={!!this.props.data.start_upload}
              >Auto Upload
              </Button>
              <Button
                bsStyle="warning"
                onClick={() => this.props.CancelFileUpload(true)}
               // disabled={!this.props.data.start_upload}
              >
              Cancel Upload
              </Button>
              { this.props.data.removable || this.props.data.cancel_upload || this.state.total_progress == 100 ?
                <Button
                  bsStyle="danger"
                  onClick={() => this.props.DeleteAllFiles()}
                >
              Remove All
                </Button>
              : null
              }
            </ButtonToolbar>
          </Col>
          <Col md={4} xs={12} style={{ textAlign: 'center' }}>
            <ProgressBar now={this.state.total_progress} />
            { this.state.total_progress == 100 ?
              <span style={{ color: 'red' }}>Completed !</span>
            : ''
            }
          </Col>
          <Col md={2} xs={12}>
            <div><b>{this.state.total_progress} %</b></div>
          </Col>
        </Row>
        <Row>
          <Col md={12} className="dropzone-container">
            <Dropzone onDrop={this.onDrop} ref={(node) => { this.dropzoneRef = node; }} className="dropzone-area">
              <h5>Try dropping some files here, or click to select files to upload.</h5>
            </Dropzone>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  files: state.common.files,
  data: state.common,
});

const mapDispatchToProps = dispatch => ({
  SetFileToStore: data => dispatch(setFiles(data)),
  StartFileUpload: value => dispatch({
    type: 'START_FILE_UPLOAD',
    value,
  }),
  CancelFileUpload: value => dispatch({
    type: 'CANCEL_FILE_UPLOAD',
    value,
  }),
  DeleteAllFiles: () => dispatch({
    type: 'DELETE_ALL_FILES',
  }),
});

Header.propTypes = {
  SetFileToStore: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

