import React from 'react';

export default class Overlay extends React.Component {
  render() {
    const { isShow, isCompleted } = this.props;

    const style = {
      display: isShow && !isCompleted ? 'block' : 'none',
    };
    return (
      <div className="timeline-wrapper" style={style}>
        <div className="timeline-item">
          <div className="animated-background" />
        </div>
      </div>
    );
  }
}
