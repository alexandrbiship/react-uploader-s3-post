import { combineReducers } from 'redux';
import common from './common';
import css from '../assets/css/style.css';

const reducers = combineReducers({
  common,
});

export default reducers;
