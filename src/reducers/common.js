const defaultState = {
  files: [],
  progress: 0,
  total_progress: [],
  start_upload: false,
  cancel_upload: false,
  removable: true,
};

const common = (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_FILES':
      state = { ...state, files: state.files.concat(action.value) };
      break;
    case 'RESET_TOTAL_PROGRESS':
      state = { ...state, total_progress: [] };
      break;
    case 'IS_REMOVABLE':
      state = { ...state, removable: action.value };
      break;
    case 'DELETE_CURRENT_FILE':
      state = { ...state, files: [...state.files.filter((file, index) => index !== action.value)], total_progress: [] };
      break;
    case 'START_FILE_UPLOAD':
      if (!state.files.length == 0) {
        state = { ...state, start_upload: action.value, cancel_upload: false };
      }
      break;
    case 'CANCEL_FILE_UPLOAD':
      // state = { ...state, cancel_upload: !state.cancel_upload };
      state = {
        ...state, cancel_upload: action.value, start_upload: false, total_progress: [],
      };
      break;
    case 'DELETE_ALL_FILES':
      state = { ...state, files: [] };
      break;
    case 'SET_TOTAL_PROGRESS':
      const data = action.value;
      let isExist = false;

      // if (state.cancel_upload) {
      //   state = { ...state, total_progress: [], cancel_upload: false };
      //   break;
      // }
      const new_total_progress = [...state.total_progress];
      new_total_progress.forEach((each) => {
        if (each.index == data.index) {
          each.percentCompleted = data.percentCompleted;
          state = { ...state, total_progress: new_total_progress, cancel_upload: false };
          isExist = true;
        }
      });
      if (!isExist) {
        state = { ...state, total_progress: [...state.total_progress.concat(data)], cancel_upload: false };
      }
      break;
    default:
      break;
  }

  return state;
};

export default common;
