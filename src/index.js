import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';

import Container from './components/Container';

class Index extends React.Component {
  render() {
    return (
      <Provider store={store} >
        <Container />
      </Provider>
    );
  }
}

const rootElement = document.getElementById('root');

ReactDOM.render(<Index />, rootElement);

